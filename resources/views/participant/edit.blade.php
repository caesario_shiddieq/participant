@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('participant.update', $participants->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="nama">Nama:</label>
          <input type="text" class="form-control" name="nama" value="{{ $participants->nama }}" />
        </div>
        <div class="form-group">
          <label for="username">Username :</label>
          <input type="text" class="form-control" name="username" value="{{ $participants->username }}" />
        </div>
        <div class="form-group">
          <label for="email">E-mail:</label>
          <input type="email" class="form-control" name="email" value="{{ $participants->email }}" />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection
