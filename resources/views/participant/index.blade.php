@extends('layout')

@section('content')

        <link  href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  <a href='participant/create' class="btn btn-default">Create</a> 
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <table class="table table-bordered table-striped" id="table">
    <thead>
        <tr>
          <th>ID</th>
          <th>Nama</th>
          <th>Username</th>
          <th>E-mail</th>
          <th colspan="2">Action</th>
        </tr>
    </thead>
   
  </table>
   <script>
         $(function() {
               $('#table').DataTable({
               processing: true,
               ajax: '{{ url('dataparticipant') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'nama', name: 'nama' },
                        { data: 'username', name: 'username' },
                        { data: 'email', name: 'email' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                     ]
            });
         });
         $('#table').on('click', '.btn-delete[data-remote]', function () {
     
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        $.ajax({
            url: url,
            type: 'DELETE',
            dataType: 'json',
            data: {method: '_DELETE', submit: true}
        }).always(function (data) {
            $('#table').DataTable().draw(true);
            window.location.reload();
        });
    });
         </script>
<div>
@endsection
