<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
        'nama',
        'username',
        'email'
    ];

    public function donation(){
    	return $this->hasMany('App\Donation');
    }

    public function biodata(){
    	return $this->hasOne('App\Biodata');
    }
}
