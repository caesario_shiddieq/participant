<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\Participant;
use App\Repositories\Repository;

class ParticipantController extends Controller
{
    protected $model;

    public function __construct(Participant $participant){
        $this->model = new Repository($participant);
    }

    public function index(){
        
        return view('participant.index');
    }

    public function dataParticipant(){
        $participant = Participant::select(['id','nama','username','email']);
        return Datatables::of($participant)
        ->addColumn('action',function($participant){
            return '<a href="'.route('participant.edit',$participant->id).'" class="btn btn-primary">Edit</a>
            <button class="btn btn-danger btn-delete" data-remote="/participant/' . $participant->id . '">Delete</button>
            '
        ;})
        ->make(true);
        
    }

    public function show(){

    }

    public function create()
    {
        return view('participant.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
        'nama'=>'required',
            'username'=>'required',
            'email'=>'required|email']);
        $this->model->create($request->only($this->model->getModel()->fillable));
        // $participant = new Participant([
        //     'nama' => $request->get('nama'),
        //     'username' => $request->get('username'),
        //     'email' => $request->get('email')
        // ]);
        // $participant->save();
        return redirect('/participant')->with('success','Participant has been saved');
    }

    public function edit($id){
        $participants = Participant::find($id);

        return view('participant.edit', compact('participants'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'nama'=>'required',
            'username'=>'required',
            'email'=>'required|email'
        ]);

        $this->model->update($request->only($this->model->getModel()->fillable), $id);

        return redirect('/participant')->with('success','Participant has been updated');
    }
    public function destroy($id){
        $this->model->delete($id);

        return redirect('/participant')->with('success','Participant has been deleted');
    }
    
}
