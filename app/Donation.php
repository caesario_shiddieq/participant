<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public function participant(){
    	return $this->belongsTo('App\Participant');
    }
}
