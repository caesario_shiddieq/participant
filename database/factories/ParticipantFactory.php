<?php

use Faker\Generator as Faker;

$factory->define(App\Participant::class, function (Faker $faker) {
        $firstName = $faker->firstName;
        $lastName = $faker->lastName;
        $username = $firstName.$lastName;
    return [

        'nama' => $firstName.' '.$lastName,
        'username' => $firstName.$lastName,
        'email' => $firstName[0].$lastName."@mail.com",
    ];
});
