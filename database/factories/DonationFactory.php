<?php

use Faker\Generator as Faker;

$factory->define(App\Donation::class, function (Faker $faker) {
    return [
        'participant_id' => rand(2,102),
        'transaction_amount' => rand(100,1000000),
    ];
});
