<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BiodataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $biodata = factory(App\Biodata::class,101)->create();
        $faker = Faker::create();

        for ($i=2; $i <103 ; $i++) { 
        	DB::table('biodata')->insert([
        		'participant_id' => $i,
        		'nik' => $faker->creditCardNumber(),
        		'kota' => $faker->city(),
        		'provinsi' => $faker->country(),
        	]);
        }
    }
}
